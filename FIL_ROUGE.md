Cours de structures de données de l'IUT d'Orléans. Conforme –on l'espère– au [PPN 2013](http://www.iut-informatique.fr/docs/ppn/fr.pdf?PHPSESSID=e803c697889f1283c297edfc3f9666da).

Pour cloner le dépôt:
```bash
git clone https://gitlab.com/FlorentBecker2/sdd
cd sdd
git config core.hooksPath .githooks
```

Ci-dessous, les notions à aborder, et les modalités.

Fils rouges
===========

Essayer d'avoir sur chaque feuille de TD/TP :

 * Un/des exercices naïfs (question de cours ou application directe du cours)
 * Une fonction nécessitant une boucle while
 * Un exercice faisant travailler la représentation de la mémoire
 * Un exercice posant des questions de performance d'un algo
 * Un exercice "défi"
 * Identifier clairement pour les étudiants ce qui est exigible à l'examen (?)

Quelques compétences à cultiver au cours du module, sans qu'elles aient un moment dédié en cours magistral:

 * Organisation du code: découper en fonction, réutiliser les fonctions déjà écrites
 * Documentation
 * Tests (utilisation via l'exerciseur; introduction à la conception?)
 * Typage
 * Justifier le fonctionnement de son code

Semaine par semaine
===================

Semaine 1
---------

### slicing ☮

_Présenter l'approche « numérotation des intervalles entre les valeurs » ; montrer les liens avec la représentation de la mémoire.

1. Présentation en cours (pas indispensable)
1. Exercices naïfs en td / tp
1. Qcm

### Représentation de la mémoire ☮☮☮

_Utiliser python tutor_ (démo en cm). Introduire le mot « objet » pour les valeurs dans le tas (cf. python tutor).

1. Présentation en cours (pas indispensable)
1. Exercices naïfs en td / tp
1. Qcm

### Données mutables et immutables ☮☮

1. Présentation en cours
1. Qcm


Semaine 2
----------


### Ensembles ☮☮

Nécessite données mutables et immutables; notation, méthodes; unicité des éléments. _Comparer la vitesse du `in` avec les listes_.


### Chronométrage ☮☮

`in` sur les listes vs les dictionnaires, boucle vs accès par `[]` sur une liste. Parler de "boucles implicites".

### Dictionnaires ☮☮☮

Clés immutables, valeurs mutables; notation, méthodes; unicité des clés.

Semaine 3
---------

_Semaine où on ne voit pas de fonction python nouvelle_

### Structures de données imbriquées ☮☮☮

1. Exercices naïfs de "suivre les crochets" (légalité, type, valeur)

### Jointures ☮☮☮

1. Introduire une représentation sous forme de graphes des clés / valeurs

Semaine 4
---------

_Révisions_

Insister sur les fils rouges en td et tp, puisqu'on a un peu de temps.

Introduire le get() en td / tp ? ☮

Semaine 5
---------

_DS_

td / tp: cf. semaine 4; commencer les notions de la semaine 6?

Semaine 6
---------

On rattaque!

### Prog. fonctionnelle ☮☮

Les fonctions sont des objets comme les autres. Passage de fonctions en argument. Fonctions locales. Formulation de problèmes « complexes » par un choix judicieux de `key=`.

1. Utilisation de `max(truc, key=bidule)`
1. Utilisation de `sorted(truc, key=bidule)`

En td/tp: utilisations naïve de sorted(), min(), max()

### Listes triées ☮☮

1. Utilisation de sorted()
1. Tri sur des listes de couples
1. Intérêt du tri comme précondition (ex: calcul du plus petit écart, avec chronométrage)

Semaine 7
---------

### Problèmes d'optimisation ☮☮

Vocabulaire, exemples, intérêt du tri pour ces problèmes.

### Recherche dichotomique ☮

Exemple d'utilisation du tri pour des raisons de performances

Semaine 8½
----------

Révisions + DS
