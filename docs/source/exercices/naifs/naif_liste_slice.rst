Slicing
-------
On considère la liste suivante :

.. code-block:: python 
   
   >>> liste=['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']


Compléter le tableau suivant :

.. csv-table:: "Tableau à compléter"
   :header: "Expression", "Valeur", "Type"
   :widths: 40, 100, 40

   `liste[3]`, "", " "
   "`liste[2:5]`", "", " "
   "`liste[-2]`", "", " "
   "`liste[0]`", "", " "
   "`liste[-2:0]`", "", " "
   "`liste[      :       ]`",  "`['c', 'd']`"," "
   "`liste[0:2]+liste[5:7]`", " ", " "
    "`+`", "`['a', 'b', 'e', 'f']`"," "


