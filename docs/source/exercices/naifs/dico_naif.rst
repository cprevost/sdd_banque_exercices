Modifications de dictionnaires
------------------------------

On peut ajouter dans un dictionnaire une clé et sa valeur associée avec:

.. code-block:: python

   >>> prix = {}
   >>> prix['stylo'] = 1.5
   >>> prix['gomme'] = 0.5
   >>> prix
   {'stylo': 1.5, 'gomme': 0.5}


Si la clé est déjà présente, on peut modifier la valeur associée:

.. code-block:: python

   >>> prix['gomme'] = 0.75
   {'stylo': 1.5, 'gomme': 0.75}

1. Ajouter un taille-crayon coûtant 0.5 au dictionnaire

2. Passer le prix du stylo à 1
