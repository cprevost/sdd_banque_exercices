.. SDD - Banque d'exercices documentation master file, created by
   sphinx-quickstart on Fri Nov  9 12:08:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


SDD - Banque d'exercices
=========================================

.. toctree::
   :caption: tous les exercices
   :name: synthese
   :maxdepth: 2
   :glob:

   feuillesTD/*/*
   
   

.. toctree::
   :caption: tous les exercices
   :name: synthese
   :maxdepth: 2
   :glob:

   exercices/*/*
   






